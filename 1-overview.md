**Mobile Development 2021/22 Portfolio**
# Overview

Your username: `c2003457`

Student ID: `2003457`

The application is a mobile network account management service. Customers can view their plan allowances, current usage and billing for the period. Bills can be downloaded and opened as PDF files. Customers can call the customer service team directly from the app. Customers can opt-in or out to ‘account overspend’ and ‘bill ready’ notifications. A loyalty rewards section lists exclusive live events for customers. Purchased tickets can be downloaded and viewed.  

Customers setup the app using their mobile number. An SMS verification code is sent to verify their identity. A PIN can be set for quicker login in the future.

Designing this app will reduce the organisations call centre wait times. The app provides a self-service platform to provide answers to common requests. Customers can access their account on-demand without having to wait in a queue, reducing frustration and providing a better user experience.

