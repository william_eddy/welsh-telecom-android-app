**Mobile Development 2021/22 Portfolio**
# Retrospective

Your username: `c2003457`

Student ID: `2003457`

Overall, I am proud to have achieved what I did in the time allocated. I have learnt an incredible amount about the Android API, discovering better and more efficient ways of achieving the same functionality. An example was when I programmatically created and formatted new rows for the billing table. If only I had known then how much quicker creating a RecyclerView was, I would have saved myself a lot of time.

Due to deadlines, I wasn’t able to implement dynamic scrolling on the events and tickets. That said, the project was starting the suffer from scope creep, particularly in the rewards section. Instead, I focused on the reliability of my existing work.

I would re-consider my API choices in the future. Volley requests had to remain in the activity or fragment, making it difficult to reuse the code. This led to some classes becoming quite cluttered and difficult to maintain. Managing permissions was also particularly challenging as without the user’s consent, the application would lose most of it’s functionality.

Finally, my confidence in using the Android framework has increased immensely throughout this assessment. I finish this task with a solid foundation of Android application development.
