package com.example.mob_dev_portfolio.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mob_dev_portfolio.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.messaging.FirebaseMessaging;

public class PasscodeCheckActivity extends AppCompatActivity {

    Button passcodeCheckContinueButton;
    EditText passcodeCheckPasscodeInput;
    TextView passcodeCheckTitle;
    TextView passcodeCheckLogoutButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passcode_check);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if(sharedPref.contains("firstName")){
            passcodeCheckTitle = findViewById(R.id.passcodeCheckTitle);
            passcodeCheckTitle.setText("Welcome back, " + sharedPref.getString("firstName",""));
        }

        passcodeCheckContinueButton = findViewById(R.id.passcodeCheckContinueButton);
        passcodeCheckPasscodeInput = findViewById(R.id.passcodeCheckPasscodeInput);
        passcodeCheckLogoutButton = findViewById(R.id.passcodeCheckLogoutButton);

        passcodeCheckContinueButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if(sharedPref.getString("passcode","").equals(passcodeCheckPasscodeInput.getText().toString())){
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(), "The passcode provided is incorrect", Toast.LENGTH_LONG).show();
                }

            }
        });

        passcodeCheckLogoutButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(view.getContext());
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.remove("passcode");
                editor.remove("firstName");
                editor.apply();

                FirebaseMessaging.getInstance().unsubscribeFromTopic("general").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getApplicationContext(),"Unsubscribed",Toast.LENGTH_LONG).show();
                    }
                });

                Intent intent = new Intent(getApplicationContext(), LoginSplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });

    }


}