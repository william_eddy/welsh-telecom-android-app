package com.example.mob_dev_portfolio.objects;

public class Ticket {

    private String ticketId;
    private String event;
    private String venue;
    private String date;
    private String orderNumber;

    public Ticket(String ticketId, String event, String venue, String date, String orderNumber) {
        this.ticketId = ticketId;
        this.event = event;
        this.venue = venue;
        this.date = date;
        this.orderNumber = orderNumber;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }
}
