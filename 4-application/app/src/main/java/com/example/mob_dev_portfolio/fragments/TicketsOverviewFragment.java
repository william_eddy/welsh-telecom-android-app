package com.example.mob_dev_portfolio.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.mob_dev_portfolio.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.ArrayList;

public class TicketsOverviewFragment extends Fragment {

    private TextView ticketDate;
    private TextView ticketName;
    private TextView ticketVenue;
    private TextView ticketOrderNumber;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_featured_events, container, false);

        Bundle data = getArguments();

        ticketDate = view.findViewById(R.id.ticketOverviewDate);
        ticketName = view.findViewById(R.id.ticketOverviewEvent);
        ticketVenue = view.findViewById(R.id.ticketOverviewVenue);
        ticketOrderNumber = view.findViewById(R.id.ticketOverviewOrderNumber);

        if (data != null){
            ticketDate.setText(data.getString("date"));
            ticketName.setText(data.getString("name"));
            ticketVenue.setText(data.getString("venue"));
            ticketOrderNumber.setText(data.getString("orderNumber"));
        }

        return view;
    }



}