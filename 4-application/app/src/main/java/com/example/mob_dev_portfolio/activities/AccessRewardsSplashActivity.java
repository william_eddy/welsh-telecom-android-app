package com.example.mob_dev_portfolio.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mob_dev_portfolio.R;

public class AccessRewardsSplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_rewards_splash);
        (new Handler()).postDelayed(this::StartRewardsHomeActivity, 2000);

    }

    public void StartRewardsHomeActivity(){
        Intent intent = new Intent(getApplicationContext(), AccessRewardsHomeActivity.class);
        startActivity(intent);
    }
}