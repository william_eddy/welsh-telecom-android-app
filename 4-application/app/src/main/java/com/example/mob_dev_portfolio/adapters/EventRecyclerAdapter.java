package com.example.mob_dev_portfolio.adapters;

import android.graphics.drawable.Drawable;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mob_dev_portfolio.objects.Event;
import com.example.mob_dev_portfolio.R;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

public class EventRecyclerAdapter extends RecyclerView.Adapter<EventRecyclerAdapter.MyViewHolder>{

    private ArrayList<Event> eventList;

    public EventRecyclerAdapter(ArrayList<Event> eventList) {
        this.eventList = eventList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView category;
        private TextView name;
        private TextView date;
        private TextView venue;
        private ImageView image;

        public MyViewHolder(final View view){
            super(view);
            category = view.findViewById(R.id.eventListItemCategory);
            name = view.findViewById(R.id.eventListItemName);
            date = view.findViewById(R.id.eventListItemDate);
            venue = view.findViewById(R.id.eventListItemVenue);
            image = view.findViewById(R.id.eventListItemImage);
        }
    }

    @NonNull
    @Override
    public EventRecyclerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_list_item,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EventRecyclerAdapter.MyViewHolder holder, int position) {
        String category = eventList.get(position).getCategory();
        holder.category.setText(category);

        String name = eventList.get(position).getName();
        holder.name.setText(name);

        String date = eventList.get(position).getDate().toString();
        holder.date.setText(date);

        String venue = eventList.get(position).getVenueName();
        holder.venue.setText(venue);

        String eventId = eventList.get(position).getEventId();

        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            holder.image.setImageDrawable(LoadImageFromWebOperations("https://welsh-telecom.herokuapp.com/images/" + eventId +".png","event_image_" + eventId));
        }
    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    public static Drawable LoadImageFromWebOperations(String url, String drawableName) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, drawableName);
            return d;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
