package com.example.mob_dev_portfolio.database;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class LocalDatabase extends SQLiteOpenHelper {

    public LocalDatabase(@Nullable Context context) {
        super(context,"WelshTelecom.db",null,1);
    }

    @Override
    public void onCreate(SQLiteDatabase DB) {
        DB.execSQL("CREATE TABLE tickets (ticketId INT PRIMARY KEY, event TEXT, venue TEXT, date TEXT, ticketNumber TEXT, orderNumber TEXT, seatSection TEXT, seatRow TEXT, seatNumber TEXT)");
        DB.execSQL("INSERT INTO tickets (ticketId, event, venue, date, ticketNumber, orderNumber, seatSection, seatRow, seatNumber) VALUES (1,'Dua-Lipa: Future Nostalgia UK Tour','Motorpoint Arena Cardiff','27th June 2022','#9631240','#89762314','F5','U','42')");
        DB.execSQL("INSERT INTO tickets (ticketId, event, venue, date, ticketNumber, orderNumber, seatSection, seatRow, seatNumber) VALUES (2,'Geroge Ezra: Live','02 Academy Bristol','14th July 2022','#8294728','#27482941','A3','C','13')");
        DB.execSQL("INSERT INTO tickets (ticketId, event, venue, date, ticketNumber, orderNumber, seatSection, seatRow, seatNumber) VALUES (3,'Declan Mckenna','02 Academy Bristol','22nd August 2022','#8347912','#68329103','D1','E','25')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase DB, int i, int i1) {
        DB.execSQL("DROP TABLE IF EXISTS tickets");
    }

    public Cursor getTicketOverviewData(){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("SELECT ticketId,date,event,venue,ticketNumber FROM tickets",null);
        return cursor;
    }

    public Cursor getTicketData(String ticketId){
        SQLiteDatabase DB = this.getWritableDatabase();
        Cursor cursor = DB.rawQuery("SELECT date,name,seatSection,seatRow,seatNumber,ticketId,orderId FROM tickets WHERE ticketId = ?",new String[] {ticketId});
        return cursor;
    }


}
