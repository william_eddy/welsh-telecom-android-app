package com.example.mob_dev_portfolio.activities;

import android.Manifest;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.mob_dev_portfolio.BuildConfig;
import com.example.mob_dev_portfolio.misc.CustomDateFormatter;
import com.example.mob_dev_portfolio.fragments.BackButtonFragment;
import com.example.mob_dev_portfolio.fragments.FragmentSubActivityHeader;
import com.example.mob_dev_portfolio.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;

public class BillingActivity extends AppCompatActivity {

    FragmentManager fragmentManager = getSupportFragmentManager();
    long downloadID;
    String invoiceNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing);

        FragmentSubActivityHeader fragmentSubActivityHeader = new FragmentSubActivityHeader();
        BackButtonFragment backButtonFragment = new BackButtonFragment();

        Bundle data = new Bundle();
        data.putString("title","Billing");
        fragmentSubActivityHeader.setArguments(data);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.planUsageMainHeaderFragmentPlaceholder, fragmentSubActivityHeader);
        fragmentTransaction.replace(R.id.billingEndOfViewBackButtonFragmentPlaceholder, backButtonFragment);
        fragmentTransaction.commit();

        registerReceiver(onDownloadComplete,new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        setCurrentBillingInfo();
        setPreviousBillingInfo();
    }


    public void setCurrentBillingInfo(){
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, "https://welsh-telecom.herokuapp.com/api/account/billing/currentPeriod", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        TextView billingTotal = findViewById(R.id.billingCurrentBillTotalText);
                        TextView monthlyCharges = findViewById(R.id.billingCurrentBillMonthlyChargesText);
                        TextView additionalCharges = findViewById(R.id.billingCurrentBillAdditionalChargesText);

                        TextView billingDate = findViewById(R.id.billingCurrentBillPeriodBillDateText);
                        TextView billingDateRange = findViewById(R.id.billingCurrentBillPeriodDateRangeText);

                        CustomDateFormatter customDateFormatter = new CustomDateFormatter();
                        DecimalFormat df = new DecimalFormat("0.00");

                        try {
                            Float monthlyChargesValue = Float.parseFloat(response.getString("monthlyCharges"));
                            Float additionalChargesValue = Float.parseFloat(response.getString("additionalCharges"));
                            Float total = monthlyChargesValue + additionalChargesValue;

                            billingTotal.setText("£" + String.valueOf(df.format(total)));
                            monthlyCharges.setText("£" + String.valueOf(df.format(monthlyChargesValue)));
                            additionalCharges.setText("£" + String.valueOf(df.format(additionalChargesValue)));

                            billingDate.setText("Your complete bill will become available from " + customDateFormatter.formatShortDate(response.getString("billingDate")));
                            billingDateRange.setText("From" + customDateFormatter.formatShortDate(response.getString("periodStart")) + " to " + customDateFormatter.formatShortDate(response.getString("periodEnd")));

                        } catch (JSONException | ParseException err) {

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "An error occurred when connecting to our services. Please check your internet connection and try again.",
                                Toast.LENGTH_LONG).show();
                    }
                }
        );
        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(getRequest);
    }


    public void setPreviousBillingInfo(){

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, "https://welsh-telecom.herokuapp.com/api/account/billing/previous", null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        CustomDateFormatter customDateFormatter = new CustomDateFormatter();
                        DecimalFormat df = new DecimalFormat("0.00");
                        JSONArray jsonArray = null;

                        try {
                            jsonArray = response;
                            int numberOfPreviousBills;

                            if(jsonArray.length() > 3){
                                numberOfPreviousBills = 3;
                            }else{
                                numberOfPreviousBills = jsonArray.length();
                            }

                            for(int i=0;i<=numberOfPreviousBills-1;i++){

                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                String billingDate = customDateFormatter.formatLongDate(jsonObject.getString("billingDate"));

                                Float monthlyChargesValue = Float.parseFloat(jsonObject.getString("monthlyCharges"));
                                Float additionalChargesValue = Float.parseFloat(jsonObject.getString("additionalCharges"));
                                Float total = monthlyChargesValue + additionalChargesValue;

                                Boolean includeBottomBorder = true;
                                if(i==numberOfPreviousBills-1){
                                    includeBottomBorder = false;
                                }

                                addPreviousBillRow(billingDate,"£" + String.valueOf(df.format(total)),jsonObject.getString("invoiceNumber"),includeBottomBorder);

                            }

                        } catch (JSONException | ParseException e) {
                            StringWriter sw = new StringWriter();
                            e.printStackTrace(new PrintWriter(sw));
                            Log.e("Error", sw.toString());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "An error occurred when connecting to our services. Please check your internet connection and try again.",
                                Toast.LENGTH_LONG).show();
                    }
                }
        );
        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(getRequest);
    }


    public void addPreviousBillRow(String date, String billTotal, String invoiceNumber, Boolean includeBottomBorder){

        TableLayout previousBillTable = findViewById(R.id.billingPreviousBillsTable);

        //create new table row
        TableRow newRow = new TableRow(this);

        //format new table row
        TableRow.LayoutParams newRowLayoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT);
        newRow.setLayoutParams(newRowLayoutParams);
        newRow.setPadding(0,2,0,4);

        if(includeBottomBorder){
            newRow.setBackgroundResource(R.drawable.table_bottom_border);
        }

        //create new textView and imageView elements for row
        TextView dateText = new TextView(this);
        TextView billText = new TextView(this);
        ImageView downloadButton = new ImageView(this);

        //set text for row and bill button image
        dateText.setText(date);
        billText.setText(billTotal);
        downloadButton.setImageResource(R.drawable.bill);

        //set download onClick listener behaviour

        downloadButton.setClickable(true);
        downloadButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                openPDFBill(invoiceNumber);
            }
        });

        //set layout params
        TableRow.LayoutParams dateLayoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,1.0f);
        dateText.setLayoutParams(dateLayoutParams);

        TableRow.LayoutParams billLayoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT,1.0f);
        billText.setLayoutParams(billLayoutParams);

        TableRow.LayoutParams imageLayoutParams = new TableRow.LayoutParams(55, 55,1.0f);
        downloadButton.setLayoutParams(imageLayoutParams);

        Typeface typeface = getResources().getFont(R.font.arial);

        //format date text
        dateText.setPadding(0,0,8,0);
        dateText.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
        dateText.setTextColor(Color.parseColor("#232F77"));
        dateText.setTextSize(TypedValue.COMPLEX_UNIT_SP,17);
        dateText.setTypeface(typeface);

        //format bill text
        billText.setPadding(6,0,7,0);
        billText.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
        billText.setTextColor(Color.parseColor("#232F77"));
        billText.setTextSize(TypedValue.COMPLEX_UNIT_SP,17);
        billText.setTypeface(typeface);

        //add elements to row
        newRow.addView(dateText);
        newRow.addView(billText);
        newRow.addView(downloadButton);

        //add row to table
        previousBillTable.addView(newRow);

    }

    public void openPDFBill(String invoiceNumber){
        this.invoiceNumber = invoiceNumber;
        String filename = "1329419_" + invoiceNumber;
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse("https://welsh-telecom.herokuapp.com/api/account/billing/downloadBill/" + invoiceNumber));
        request.setTitle(filename);
        request.setMimeType("application/pdf");
        request.allowScanningByMediaScanner();
        request.setAllowedOverMetered(true);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,filename);
        DownloadManager dm= (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        downloadID = dm.enqueue(request);
    }


    private BroadcastReceiver onDownloadComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //get the download ID received by the broadcast
            long broadcastId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            //check if broadcast ID is equal to the download ID produced when the download was added to the queue
            if (downloadID == broadcastId) {

                try {

                    DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                    Uri uri = manager.getUriForDownloadedFile(downloadID);

                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + "/1329419_"
                            + invoiceNumber + ".pdf");

                    Intent pdfIntent = new Intent(Intent.ACTION_VIEW);

                    pdfIntent.setDataAndType(uri, "application/pdf");
                    pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                    String title = "Open bill in";
                    // Create intent to show the chooser dialog
                    Intent chooser = Intent.createChooser(pdfIntent, title);

                    startActivity(chooser);

                }catch(java.lang.SecurityException e){
                    Toast.makeText(getApplicationContext(), "File permissions are required to open bills. Please manually enable the permission in the settings menu.", Toast.LENGTH_LONG).show();
                }
            }
        }
    };



}