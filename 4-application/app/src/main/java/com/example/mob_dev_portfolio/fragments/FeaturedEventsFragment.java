package com.example.mob_dev_portfolio.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.mob_dev_portfolio.misc.CustomDateFormatter;
import com.example.mob_dev_portfolio.objects.Event;
import com.example.mob_dev_portfolio.adapters.EventRecyclerAdapter;
import com.example.mob_dev_portfolio.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.ArrayList;

public class FeaturedEventsFragment extends Fragment {

    private ArrayList<Event> eventList;
    private RecyclerView recyclerView;
    private EventRecyclerAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_featured_events, container, false);

        recyclerView = view.findViewById(R.id.featuredEventsRecyclerView);

        eventList = new ArrayList<>();
        adapter = new EventRecyclerAdapter(eventList);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.LayoutManager layoutManager;

        layoutManager = new LinearLayoutManager(getContext()){
            @Override
            public boolean canScrollVertically() {
                return true;
            }
        };

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);


        setEventList();

        return view;
    }



    public void setEventList(){

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, "https://welsh-telecom.herokuapp.com/api/rewards/events", null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        CustomDateFormatter customDateFormatter = new CustomDateFormatter();
                        JSONArray jsonArray = null;

                        try {
                            jsonArray = response;

                            for(int i=0;i<=jsonArray.length()-1;i++){

                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                String event_id = jsonObject.getString("event_id");
                                String name = jsonObject.getString("name");
                                LocalDate date = LocalDate.parse(jsonObject.getString("date"));
                                String venue = jsonObject.getString("venue");
                                String category = jsonObject.getString("category");

                                eventList.add(new Event(event_id,name,date,category,venue));

                            }

                        } catch (JSONException e) {
                            StringWriter sw = new StringWriter();
                            e.printStackTrace(new PrintWriter(sw));
                            Log.e("Error", sw.toString());
                        }
                        adapter.notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "An error occurred when connecting to our services. Please check your internet connection and try again.",
                                Toast.LENGTH_LONG).show();
                    }
                }
        );
        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(getRequest);
    }


}