package com.example.mob_dev_portfolio.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.mob_dev_portfolio.adapters.TicketRecyclerAdapter;
import com.example.mob_dev_portfolio.fragments.FeaturedEventsFragment;
import com.example.mob_dev_portfolio.fragments.MyTicketsFragment;
import com.example.mob_dev_portfolio.R;

public class AccessRewardsHomeActivity extends AppCompatActivity{

    private ImageView backButton;
    private FragmentManager fragmentManager = getSupportFragmentManager();
    private TextView featuredEventsButton;
    private TextView myTicketsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_rewards_home);

        loadFeaturedEvents();

        backButton = findViewById(R.id.rewardsHomeBackButton);
        backButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        featuredEventsButton = findViewById(R.id.rewardsHomeFeaturedEventsButton);
        myTicketsButton = findViewById(R.id.rewardsHomeMyTicketsButton);

        featuredEventsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                loadFeaturedEvents();
                myTicketsButton.setTextColor(getResources().getColor(R.color.unselected_button_colour));
                featuredEventsButton.setTextColor(getResources().getColor(R.color.white));
            }
        });

        myTicketsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                loadMyTickets();
                myTicketsButton.setTextColor(getResources().getColor(R.color.white));
                featuredEventsButton.setTextColor(getResources().getColor(R.color.unselected_button_colour));
            }
        });

    }

    private void loadFeaturedEvents(){
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.rewardsHomeContentPlaceholder,new FeaturedEventsFragment());
        fragmentTransaction.commit();
    }

    private void loadMyTickets(){
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.rewardsHomeContentPlaceholder,new MyTicketsFragment());
        fragmentTransaction.commit();
    }
}