package com.example.mob_dev_portfolio.activities;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.mob_dev_portfolio.R;
import com.example.mob_dev_portfolio.database.LocalDatabase;
import com.example.mob_dev_portfolio.fragments.BackButtonFragment;
import com.example.mob_dev_portfolio.fragments.FeaturedEventsFragment;
import com.example.mob_dev_portfolio.fragments.MyTicketsFragment;
import com.example.mob_dev_portfolio.objects.Ticket;

public class AccessRewardsTicketActivity extends AppCompatActivity {

    private LinearLayout backButton;
    private FragmentManager fragmentManager = getSupportFragmentManager();

    private TextView eventDateText;
    private TextView eventNameText;
    private TextView sectionText;
    private TextView rowText;
    private TextView seatText;
    private TextView ticketNumberText;
    private TextView orderNumberText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_rewards_ticket);

        eventDateText = findViewById(R.id.ticketHeaderDateText);
        eventNameText = findViewById(R.id.ticketHeaderEventText);

        sectionText = findViewById(R.id.ticketSectionText);
        rowText = findViewById(R.id.ticketRowText);
        seatText = findViewById(R.id.ticketSeatText);

        ticketNumberText = findViewById(R.id.ticketNumberText);
        orderNumberText = findViewById(R.id.ticketOrderNumberText);

        Bundle b = getIntent().getExtras();
        String ticketId = ""; // or other values
        if(b != null)
            ticketId = b.getString("ticketId");

        LocalDatabase db = new LocalDatabase(getApplicationContext());
        Cursor res = db.getTicketData(ticketId);

        while(res.moveToNext()){
            eventDateText.setText(res.getString(1));
            eventNameText.setText(res.getString(2));

            sectionText.setText(res.getString(3));
            rowText.setText(res.getString(4));
            seatText.setText(res.getString(5));

            ticketNumberText.setText(res.getString(6));
            orderNumberText.setText(res.getString(7));
        }

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.ticketEndOfViewBackButtonFragmentPlaceholder, new BackButtonFragment());
        fragmentTransaction.commit();

        backButton = findViewById(R.id.ticketBackButtonInner);
        backButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

}