package com.example.mob_dev_portfolio.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.mob_dev_portfolio.R;

import org.json.JSONException;
import org.json.JSONObject;

public class FragmentSubActivityHeader extends Fragment {

    private String title = "Your Account";
    private TextView titleTextView;
    private LinearLayout backButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sub_header, container, false);
        titleTextView = view.findViewById(R.id.subHeaderTitle);

        setCustomerInfo();

        Bundle data = getArguments();

        if (data != null){
            title = data.getString("title");
        }

        titleTextView.setText(title);

        backButton = view.findViewById(R.id.subHeaderBackButton);
        backButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });

        return view;
    }

    public void setCustomerInfo(){

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, "https://welsh-telecom.herokuapp.com/api/account/customer", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        TextView customerName = getView().findViewById(R.id.subHeaderAccountName);
                        TextView customerPhoneNumber = getView().findViewById(R.id.subHeaderPhoneNumber);
                        try {
                            customerName.setText(response.getString("firstName") + " " + response.getString("lastName"));
                            customerPhoneNumber.setText(response.getString("phoneNumber"));
                        } catch (JSONException err) {

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "An error occurred when connecting to our services. Please check your internet connection and try again.",
                                Toast.LENGTH_LONG).show();
                    }
                }
        );
        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(getRequest);

    }
}