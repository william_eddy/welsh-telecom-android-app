package com.example.mob_dev_portfolio.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.mob_dev_portfolio.activities.LoginActivity;
import com.example.mob_dev_portfolio.R;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginMobileNumberFormFragment extends Fragment {

    Button loginMobileNumberContinueButton;
    EditText loginMobileNumberInput;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login_mobile_number_form, container, false);
        loginMobileNumberContinueButton = view.findViewById(R.id.loginMobileNumberContinueButton);

        loginMobileNumberInput = view.findViewById(R.id.loginMobileNumberInput);

        loginMobileNumberContinueButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                verifyMobileNumber(loginMobileNumberInput.getText().toString());
            }
        });

        return view;
    }

    public void verifyMobileNumber(String mobileNumber){

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, "https://welsh-telecom.herokuapp.com/api/login/" + mobileNumber, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                ((LoginActivity) getActivity()).replaceMobileNumberFormWithOtpForm();
                            }else{
                                Toast.makeText(getActivity(), "The number provided is not associated with a Welsh Telecom account",
                                        Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException err) {

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "An error occurred when connecting to our services. Please check your internet connection and try again.",
                                Toast.LENGTH_LONG).show();
                    }
                }
        );

        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(getRequest);

    }

}