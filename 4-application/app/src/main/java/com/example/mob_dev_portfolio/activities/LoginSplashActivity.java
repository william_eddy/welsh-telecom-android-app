package com.example.mob_dev_portfolio.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;

import com.example.mob_dev_portfolio.R;

public class LoginSplashActivity extends AppCompatActivity {

    Button splashSignInbutton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_splash);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if(sharedPref.contains("passcode")){
            Intent intent = new Intent(this, PasscodeCheckActivity.class);
            startActivity(intent);
        }

        splashSignInbutton = findViewById(R.id.splashSignInbutton);
        splashSignInbutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PermissionsActivity.class);
                startActivity(intent);
            }
        });

    }
}