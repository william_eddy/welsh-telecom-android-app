package com.example.mob_dev_portfolio.misc;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomDateFormatter {

    public String formatShortDate(String date) throws ParseException {
        DateFormat parser = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat formatter = new SimpleDateFormat("dd MMM");
        Date convertedDate = parser.parse(date);
        return formatter.format(convertedDate);
    }

    public String formatLongDate(String date) throws ParseException {
        DateFormat parser = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        Date convertedDate = parser.parse(date);
        return formatter.format(convertedDate);
    }

}

