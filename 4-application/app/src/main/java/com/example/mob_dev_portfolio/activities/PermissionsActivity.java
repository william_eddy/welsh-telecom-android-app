package com.example.mob_dev_portfolio.activities;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mob_dev_portfolio.R;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class PermissionsActivity extends AppCompatActivity {

    Switch smsPermissionSwitch;
    Switch internetPermissionSwitch;
    Switch readWritePermissionSwitch;
    Switch phonePermissionSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions);

        checkPermissionsAreMet();

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        smsPermissionSwitch = findViewById(R.id.permissionsSmsSwitch);
        internetPermissionSwitch = findViewById(R.id.permissionsInternetSwitch);
        readWritePermissionSwitch = findViewById(R.id.permissionsReadWriteSwitch);
        phonePermissionSwitch = findViewById(R.id.permissionsPhoneSwitch);

        smsPermissionSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(permissionStatus(Manifest.permission.SEND_SMS) == false && isChecked){
                    requestPermission(Manifest.permission.SEND_SMS);
                }
                SharedPreferences.Editor editor = sharedPref.edit();
                if(permissionStatus(Manifest.permission.SEND_SMS)){
                    smsPermissionSwitch.setChecked(true);
                }else{
                    if(!sharedPref.contains("smsAttemptCount")){
                        editor.putInt("smsAttemptCount", 1);
                    }else{
                        editor.putInt("smsAttemptCount", sharedPref.getInt("smsAttemptCount",0) + 1);
                    }
                    editor.apply();
                    smsPermissionSwitch.setChecked(false);
                }
                if(sharedPref.getInt("smsAttemptCount",0)>2){
                    generateManualPermissionToast();
                }
            }
        });


        internetPermissionSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(permissionStatus(Manifest.permission.INTERNET) == false && isChecked){
                    requestPermission(Manifest.permission.INTERNET);
                }
                SharedPreferences.Editor editor = sharedPref.edit();
                if(permissionStatus(Manifest.permission.INTERNET)){
                    internetPermissionSwitch.setChecked(true);
                }else{
                    if(!sharedPref.contains("internetAttemptCount")){
                        editor.putInt("internetAttemptCount", 1);
                    }else{
                        editor.putInt("internetAttemptCount", sharedPref.getInt("internetAttemptCount",0) + 1);
                    }
                    editor.apply();
                    internetPermissionSwitch.setChecked(false);
                }
                if(sharedPref.getInt("internetAttemptCount",0)>2){
                    generateManualPermissionToast();
                }
            }
        });


        readWritePermissionSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if((permissionStatus(Manifest.permission.WRITE_EXTERNAL_STORAGE) == false) && isChecked){
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                }
                SharedPreferences.Editor editor = sharedPref.edit();
                if((permissionStatus(Manifest.permission.WRITE_EXTERNAL_STORAGE))){
                    readWritePermissionSwitch.setChecked(true);
                }else{
                    if(!sharedPref.contains("readWriteAttemptCount")){
                        editor.putInt("readWriteAttemptCount", 1);
                    }else{
                        editor.putInt("readWriteAttemptCount", sharedPref.getInt("readWriteAttemptCount",0) + 1);
                    }
                    editor.apply();
                    readWritePermissionSwitch.setChecked(false);
                }
                if(sharedPref.getInt("readWriteAttemptCount",0)>2){
                    generateManualPermissionToast();
                }
            }
        });

        phonePermissionSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(permissionStatus(Manifest.permission.READ_PHONE_STATE) == false && isChecked){
                    requestPermission(Manifest.permission.READ_PHONE_STATE);
                }
                SharedPreferences.Editor editor = sharedPref.edit();
                if(permissionStatus(Manifest.permission.READ_PHONE_STATE)){
                    phonePermissionSwitch.setChecked(true);
                }else{
                    if(!sharedPref.contains("phoneAttemptCount")){
                        editor.putInt("phoneAttemptCount", 1);
                    }else{
                        editor.putInt("phoneAttemptCount", sharedPref.getInt("phoneAttemptCount",0) + 1);
                    }
                    editor.apply();
                    phonePermissionSwitch.setChecked(false);
                }
                if(sharedPref.getInt("phoneAttemptCount",0)>2){
                    generateManualPermissionToast();
                }
            }
        });

    }


    private Boolean permissionStatus(String permission){
        if(checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        return false;
    }

    private void requestPermission(String permission){
        requestPermissions(new String[]{permission}, 0);
    }

    private void generateManualPermissionToast(){
        Toast.makeText(getApplicationContext(), "You have denied the request too many times. Please manually enable the permission in the settings menu.", Toast.LENGTH_LONG).show();
    }

    private void checkPermissionsAreMet() {

        final Handler handler = new Handler();
        final int delay = 1000; // 1000 milliseconds == 1 second

        handler.postDelayed(new Runnable() {
            volatile boolean shutdown = false;

            public void run() {

                if (shutdown == false) {

                    if (permissionStatus(Manifest.permission.SEND_SMS)) {
                        smsPermissionSwitch.setChecked(true);
                    } else {
                        smsPermissionSwitch.setChecked(false);
                    }

                    if (permissionStatus(Manifest.permission.INTERNET)) {
                        internetPermissionSwitch.setChecked(true);
                    } else {
                        internetPermissionSwitch.setChecked(false);
                    }


                    if(permissionStatus(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                        readWritePermissionSwitch.setChecked(true);
                    }else {
                        readWritePermissionSwitch.setChecked(false);
                    }

                    if (permissionStatus(Manifest.permission.READ_PHONE_STATE)) {
                        phonePermissionSwitch.setChecked(true);
                    } else {
                        phonePermissionSwitch.setChecked(false);
                    }

                    if (permissionStatus(Manifest.permission.SEND_SMS) && permissionStatus(Manifest.permission.INTERNET) && permissionStatus(Manifest.permission.READ_PHONE_STATE) && permissionStatus(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        Intent intent = new Intent(getApplicationContext(), ConnectingActivity.class);
                        startActivity(intent);
                        shutdown = true;
                    }
                }

                handler.postDelayed(this, delay);
            }
        }, delay);
    }
}