package com.example.mob_dev_portfolio.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.mob_dev_portfolio.notifications.PushNotificationService;
import com.example.mob_dev_portfolio.R;

import org.json.JSONException;
import org.json.JSONObject;

public class PasscodeCreationActivity extends AppCompatActivity {

    Button passcodeCreationFormContinueButton;
    EditText passcodeCreationFormInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passcode_creation);

        passcodeCreationFormContinueButton = findViewById(R.id.passcodeCreationFormContinueButton);
        passcodeCreationFormInput = findViewById(R.id.passcodeCreationFormPasscodeInput);

        passcodeCreationFormContinueButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                String passcode = passcodeCreationFormInput.getText().toString();

                if(passcode.length()!=4){
                    Toast.makeText(getApplicationContext(), "The passcode must be 4 digits exactly", Toast.LENGTH_LONG).show();
                }else{
                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString("passcode", passcode);
                    editor.putBoolean("overspendNotification", true);
                    editor.putBoolean("billReadyNotification", true);
                    editor.putString("passcode", passcode);
                    editor.commit();

                    PushNotificationService pushNotificationService = new PushNotificationService();
                    pushNotificationService.subscribeToNotifications("overspend");
                    pushNotificationService.subscribeToNotifications("billReady");

                    setFirstNameSharedPref();

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intent);
                }
            }
        });

    }

    public void setFirstNameSharedPref(){
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, "https://welsh-telecom.herokuapp.com/api/account/customer", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString("firstName", response.getString("firstName"));
                            editor.commit();
                        } catch (JSONException err) {

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "An error occurred when connecting to our services. Please check your internet connection and try again.",
                                Toast.LENGTH_LONG).show();
                    }
                }
        );
        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(getRequest);
    }


}