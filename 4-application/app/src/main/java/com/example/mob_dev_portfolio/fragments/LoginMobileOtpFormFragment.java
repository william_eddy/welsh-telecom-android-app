package com.example.mob_dev_portfolio.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.example.mob_dev_portfolio.activities.LoginActivity;
import com.example.mob_dev_portfolio.activities.PermissionsActivity;
import com.example.mob_dev_portfolio.R;

import java.util.Random;

public class LoginMobileOtpFormFragment extends Fragment {

    Button loginOtpFormContinueButton;
    EditText otpCodeInput;
    ImageView loginOtpFormPadlock;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login_otp_form, container, false);


        if(!(ActivityCompat.checkSelfPermission(getActivity(),Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
        || ActivityCompat.checkSelfPermission(getActivity(),Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED)) {
            Intent intent = new Intent(view.getContext(), PermissionsActivity.class);
            startActivity(intent);
        }

        String otpCode = generateRandomOtpCode();

        if(ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.SEND_SMS}, 0);
        }

        try {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage("5554", null, "YOUR CODE IS " + otpCode, null, null);
        }catch(java.lang.SecurityException e){
            Toast.makeText(getContext(), "SMS permissions are required to use this application. Please manually enable the permission in the settings menu.", Toast.LENGTH_LONG).show();
        }


        otpCodeInput = view.findViewById(R.id.loginOtpFormCodeInput);

        loginOtpFormContinueButton = view.findViewById(R.id.loginOtpFormContinueButton);
        loginOtpFormContinueButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if(otpCodeInput.getText().toString().equals(otpCode)){
                    ((LoginActivity) getActivity()).otpSuccess();
                }else{
                    Toast.makeText(getContext(), "The code entered is incorrect", Toast.LENGTH_LONG).show();
                }

            }
        });

        loginOtpFormPadlock = view.findViewById(R.id.loginOtpFormPadlock);
        loginOtpFormPadlock.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                ((LoginActivity) getActivity()).otpSuccess();
            }
        });

        return view;
    }

    private String generateRandomOtpCode(){
        Random r = new Random();
        int lowerBound = 111111;
        int upperBound = 999999;
        int code = r.nextInt(upperBound-lowerBound) + lowerBound;
        return String.valueOf(code);
    }
}