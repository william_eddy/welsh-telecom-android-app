package com.example.mob_dev_portfolio.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.mob_dev_portfolio.R;
import com.example.mob_dev_portfolio.fragments.BackButtonFragment;
import com.example.mob_dev_portfolio.fragments.FragmentSubActivityHeader;

import org.json.JSONException;
import org.json.JSONObject;

public class PlanUsageActivity extends AppCompatActivity {

    FragmentManager fragmentManager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_usage);

        FragmentSubActivityHeader fragmentSubActivityHeader = new FragmentSubActivityHeader();
        BackButtonFragment backButtonFragment = new BackButtonFragment();

        Bundle data = new Bundle();
        data.putString("title","Your Plan");
        fragmentSubActivityHeader.setArguments(data);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.planUsageMainHeaderFragmentPlaceholder, fragmentSubActivityHeader);
        fragmentTransaction.replace(R.id.planUsageEndOfViewBackButtonFragmentPlaceholder, backButtonFragment);
        fragmentTransaction.commit();

        setUsageInfo();

    }


    public void setUsageInfo(){
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, "https://welsh-telecom.herokuapp.com/api/account/usage", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        TextView dataAllowance = findViewById(R.id.planUsageDataAllowanceAmountText);
                        TextView dataUsed = findViewById(R.id.planUsageDataUsedAmountText);
                        ProgressBar dataProgressBar = findViewById(R.id.planUsageDataProgressBar);

                        TextView textsAllowance = findViewById(R.id.planUsageTextAllowanceAmountText);
                        TextView textsUsed = findViewById(R.id.planUsageTextUsedAmountText);
                        ProgressBar textsProgressBar = findViewById(R.id.planUsageTextProgressBar);

                        TextView minutesAllowance = findViewById(R.id.planUsageMinutesAllowanceAmountText);
                        TextView minutesUsed = findViewById(R.id.planUsageMinutesUsedAmountText);
                        ProgressBar minutesProgressBar = findViewById(R.id.planUsageMinutesProgressBar);

                        try {

                            String dataAllowanceValue = response.getString("dataAllowance");
                            String dataUsedValue = response.getString("dataUsed");
                            dataAllowance.setText(dataAllowanceValue+ "GB");
                            dataUsed.setText(dataUsedValue + "GB");
                            dataProgressBar.setProgress(Math.round((100/Float.parseFloat(dataAllowanceValue)*Float.parseFloat(dataUsedValue))));

                            String textsAllowanceValue = response.getString("textsAllowance");
                            String textsUsedValue = response.getString("textsUsed");
                            textsAllowance.setText(textsAllowanceValue);
                            textsUsed.setText(textsUsedValue);
                            textsProgressBar.setProgress(Math.round((100/Float.parseFloat(textsAllowanceValue)*Float.parseFloat(textsUsedValue))));

                            String minutesAllowanceValue = response.getString("minutesAllowance");
                            String minutesUsedValue = response.getString("minutesUsed");
                            minutesAllowance.setText(minutesAllowanceValue);
                            minutesUsed.setText(minutesUsedValue);
                            minutesProgressBar.setProgress(Math.round((100/Float.parseFloat(minutesAllowanceValue)*Float.parseFloat(minutesUsedValue))));

                        } catch (JSONException err) {

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "An error occurred when connecting to our services. Please check your internet connection and try again.",
                                Toast.LENGTH_LONG).show();
                    }
                }
        );
        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(getRequest);
    }


}