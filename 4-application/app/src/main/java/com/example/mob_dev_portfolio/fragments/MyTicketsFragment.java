package com.example.mob_dev_portfolio.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mob_dev_portfolio.activities.AccessRewardsTicketActivity;
import com.example.mob_dev_portfolio.database.LocalDatabase;
import com.example.mob_dev_portfolio.R;
import com.example.mob_dev_portfolio.objects.Ticket;
import com.example.mob_dev_portfolio.adapters.TicketRecyclerAdapter;

import java.util.ArrayList;

public class MyTicketsFragment extends Fragment implements TicketRecyclerAdapter.OnNoteListener{

    private ArrayList<Ticket> ticketList;
    private RecyclerView recyclerView;
    private TicketRecyclerAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_tickets, container, false);

        recyclerView = view.findViewById(R.id.myTicketsRecyclerView);

        ticketList = new ArrayList<>();


        LocalDatabase db = new LocalDatabase(getContext());
        Cursor res = db.getTicketOverviewData();

        while(res.moveToNext()){

            String ticketId = res.getString(0);
            String date = res.getString(1);
            String event = res.getString(2);
            String venue = res.getString(3);
            String ticketNumber = res.getString(4);

            ticketList.add(new Ticket(ticketId,date,event,venue,ticketNumber));
        }

        adapter = new TicketRecyclerAdapter(ticketList, this);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.LayoutManager layoutManager;

        layoutManager = new LinearLayoutManager(getContext()){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onNoteClick(int position) {
        Intent intent = new Intent(getActivity(), AccessRewardsTicketActivity.class);
        Bundle b = new Bundle();
        b.putInt("ticketId", position+1); //Your id
        intent.putExtras(b); //Put your id to your next Intent
        startActivity(intent);
    }

}