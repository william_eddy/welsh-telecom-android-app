package com.example.mob_dev_portfolio.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mob_dev_portfolio.R;

public class OtpSuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_success);

        (new Handler()).postDelayed(this::StartPasscodeCreationActivity, 3000);

    }

    public void StartPasscodeCreationActivity(){
        Intent intent = new Intent(getApplicationContext(), PasscodeCreationActivity.class);
        startActivity(intent);
    }

}