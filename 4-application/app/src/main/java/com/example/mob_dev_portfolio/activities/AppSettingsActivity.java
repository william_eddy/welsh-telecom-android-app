package com.example.mob_dev_portfolio.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.CompoundButton;
import android.widget.Switch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.mob_dev_portfolio.fragments.BackButtonFragment;
import com.example.mob_dev_portfolio.fragments.FragmentSubActivityHeader;
import com.example.mob_dev_portfolio.notifications.PushNotificationService;
import com.example.mob_dev_portfolio.R;

public class AppSettingsActivity extends AppCompatActivity {

    FragmentManager fragmentManager = getSupportFragmentManager();
    Switch overspendingSwitch;
    Switch billReadySwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_settings);

        FragmentSubActivityHeader fragmentSubActivityHeader = new FragmentSubActivityHeader();
        BackButtonFragment backButtonFragment = new BackButtonFragment();

        Bundle data = new Bundle();
        data.putString("title","App Settings");
        fragmentSubActivityHeader.setArguments(data);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.appSettingsMainHeaderFragmentPlaceholder, fragmentSubActivityHeader);
        fragmentTransaction.replace(R.id.appSettingsEndOfViewBackButtonFragmentPlaceholder, backButtonFragment);
        fragmentTransaction.commit();

        overspendingSwitch = findViewById(R.id.appSettingsOverspendingToggleSwitch);
        billReadySwitch = findViewById(R.id.appSettingsNotificationsBillReadyToDownloadTitleToggleSwitch);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if(sharedPref.contains("overspendNotification") && sharedPref.getBoolean("overspendNotification",true)){
            overspendingSwitch.setChecked(true);
        }else{
            overspendingSwitch.setChecked(false);
        }

        if(sharedPref.contains("billReadyNotification") && sharedPref.getBoolean("billReadyNotification",true)){
            billReadySwitch.setChecked(true);
        }else{
            billReadySwitch.setChecked(false);
        }

        PushNotificationService pushNotificationService = new PushNotificationService();

        overspendingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = sharedPref.edit();
                if(isChecked){
                    editor.putBoolean("overspendNotification", true);
                    pushNotificationService.subscribeToNotifications("overspend");
                }else{
                    editor.putBoolean("overspendNotification", false);
                    pushNotificationService.unsubscribeToNotifications("overspend");
                }
                editor.commit();
            }
        });

        billReadySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = sharedPref.edit();
                if(isChecked){
                    editor.putBoolean("billReadyNotification", true);
                    pushNotificationService.subscribeToNotifications("billReady");
                }else{
                    editor.putBoolean("billReadyNotification", false);
                    pushNotificationService.unsubscribeToNotifications("billReady");
                }
                editor.commit();
            }
        });

    }


}