package com.example.mob_dev_portfolio.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mob_dev_portfolio.R;
import com.example.mob_dev_portfolio.objects.Ticket;

import java.util.ArrayList;

public class TicketRecyclerAdapter extends RecyclerView.Adapter<TicketRecyclerAdapter.MyViewHolder>{

    private ArrayList<Ticket> ticketList;
    private OnNoteListener mOnNoteListener;

    public TicketRecyclerAdapter(ArrayList<Ticket> ticketList, OnNoteListener onNoteListener) {
        this.ticketList = ticketList;
        this.mOnNoteListener = onNoteListener;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView date;
        private TextView event;
        private TextView venue;
        private TextView orderNumber;
        OnNoteListener onNoteListener;

        public MyViewHolder(final View view, OnNoteListener onNoteListener){
            super(view);
            date = view.findViewById(R.id.ticketOverviewDate);
            event = view.findViewById(R.id.ticketOverviewEvent);
            venue = view.findViewById(R.id.ticketOverviewVenue);
            orderNumber = view.findViewById(R.id.ticketOverviewOrderNumber);
            this.onNoteListener = onNoteListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            onNoteListener.onNoteClick(getAdapterPosition());
        }
    }

    @NonNull
    @Override
    public TicketRecyclerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ticket_list_item,parent,false);
        return new MyViewHolder(itemView, mOnNoteListener);
    }

    @Override
    public void onBindViewHolder(@NonNull TicketRecyclerAdapter.MyViewHolder holder, int position) {
        String date = ticketList.get(position).getDate();
        holder.date.setText(date);

        String event = ticketList.get(position).getEvent();
        holder.event.setText(event);

        String venue = ticketList.get(position).getVenue();
        holder.venue.setText(venue);

        String orderNumber = ticketList.get(position).getOrderNumber();
        holder.orderNumber.setText(orderNumber);
    }


    @Override
    public int getItemCount() {
        return ticketList.size();
    }

    public interface OnNoteListener{
        void onNoteClick(int position);
    }
}
