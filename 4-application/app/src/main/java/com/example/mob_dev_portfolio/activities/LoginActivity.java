package com.example.mob_dev_portfolio.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mob_dev_portfolio.R;
import com.example.mob_dev_portfolio.fragments.LoginMobileNumberFormFragment;
import com.example.mob_dev_portfolio.fragments.LoginMobileOtpFormFragment;

public class LoginActivity extends AppCompatActivity {

    LinearLayout loginBackButton;
    FragmentManager fragmentManager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        FragmentTransaction mobileFormFragmentTransaction = fragmentManager.beginTransaction();
        mobileFormFragmentTransaction.replace(R.id.loginFragmentPlaceholder,new LoginMobileNumberFormFragment());
        mobileFormFragmentTransaction.commit();

        loginBackButton = findViewById(R.id.loginBackButton);

        loginBackButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    public void replaceMobileNumberFormWithOtpForm(){
        TextView loginTitle = (TextView)findViewById(R.id.loginTitle);
        loginTitle.setText("We’ve sent a text to your phone");

        FragmentTransaction otpFormFragmentTransaction = fragmentManager.beginTransaction();
        otpFormFragmentTransaction.replace(R.id.loginFragmentPlaceholder,new LoginMobileOtpFormFragment());
        otpFormFragmentTransaction.commit();
    }

    public void otpSuccess(){
        Intent intent = new Intent(this, OtpSuccessActivity.class);
        startActivity(intent);
    }

    public void otpPermissionFailure(){
        Intent intent = new Intent(this, PermissionsActivity.class);
        startActivity(intent);
    }

}