package com.example.mob_dev_portfolio.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.mob_dev_portfolio.fragments.BackButtonFragment;
import com.example.mob_dev_portfolio.fragments.FragmentSubActivityHeader;
import com.example.mob_dev_portfolio.R;

public class ContactUsActivity extends AppCompatActivity {

    FragmentManager fragmentManager = getSupportFragmentManager();
    Button callUsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        FragmentSubActivityHeader fragmentSubActivityHeader = new FragmentSubActivityHeader();
        BackButtonFragment backButtonFragment = new BackButtonFragment();

        Bundle data = new Bundle();
        data.putString("title","Contact Us");
        fragmentSubActivityHeader.setArguments(data);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.contactUsMainHeaderFragmentPlaceholder, fragmentSubActivityHeader);
        fragmentTransaction.replace(R.id.contactUsEndOfViewBackButtonFragmentPlaceholder, backButtonFragment);
        fragmentTransaction.commit();

        callUsButton = findViewById(R.id.contactUsCallUsButton);

        callUsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Uri number = Uri.parse("tel:03331036969");
                Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
                startActivity(callIntent);

            }
        });


    }


}