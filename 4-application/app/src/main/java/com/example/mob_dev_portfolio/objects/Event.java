package com.example.mob_dev_portfolio.objects;

import java.sql.Time;
import java.time.LocalDate;
import java.util.Date;

public class Event {

    private String eventId;
    private String name;
    private LocalDate date;
    private String category;
    private String venueName;

    public Event(String eventId, String name, LocalDate date, String category, String venueName) {
        this.eventId = eventId;
        this.name = name;
        this.date = date;
        this.category = category;
        this.venueName = venueName;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }
}
