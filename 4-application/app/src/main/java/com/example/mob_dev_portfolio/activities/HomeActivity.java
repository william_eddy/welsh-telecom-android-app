package com.example.mob_dev_portfolio.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.mob_dev_portfolio.misc.CustomDateFormatter;
import com.example.mob_dev_portfolio.R;
import com.example.mob_dev_portfolio.fragments.HomeActivityHeaderFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;

public class HomeActivity extends AppCompatActivity {

    FragmentManager fragmentManager = getSupportFragmentManager();
    FrameLayout yourPlanContainerParent;
    FrameLayout billingContainerParent;
    FrameLayout contactUsContainerParent;
    FrameLayout appSettingsContainerParent;
    FrameLayout rewardsContainerParent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        HomeActivityHeaderFragment homeActivityHeaderFragment = new HomeActivityHeaderFragment();

        yourPlanContainerParent = findViewById(R.id.homeYourPlanContainerParent);
        billingContainerParent = findViewById(R.id.homeBillingContainerParent);
        contactUsContainerParent = findViewById(R.id.homeContactUsContainerParent);
        appSettingsContainerParent  = findViewById(R.id.homeAppSettingsContainerParent);
        rewardsContainerParent  = findViewById(R.id.homeAppRewardsContainerParent);


        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.homeHeaderFragmentPlaceholder, homeActivityHeaderFragment);
        fragmentTransaction.commit();

        setUsageInfo();
        setBillingInfo();

        yourPlanContainerParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), PlanUsageActivity.class);
                startActivity(intent);

            }
        });

        rewardsContainerParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), AccessRewardsSplashActivity.class);
                startActivity(intent);

            }
        });

        billingContainerParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), BillingActivity.class);
                startActivity(intent);

            }
        });

        contactUsContainerParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), ContactUsActivity.class);
                startActivity(intent);

            }
        });

        appSettingsContainerParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), AppSettingsActivity.class);
                startActivity(intent);

            }
        });

    }

    public void setUsageInfo(){
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, "https://welsh-telecom.herokuapp.com/api/account/usage", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        TextView dataAllowance = findViewById(R.id.homeYourPlanUsageDataAllowanceText);
                        TextView dataUsed = findViewById(R.id.homeYourPlanUsageDataUsedText);

                        TextView textsAllowance = findViewById(R.id.homeYourPlanUsageTextsAllowanceText);
                        TextView textsUsed = findViewById(R.id.homeYourPlanUsageTextsUsedText);

                        TextView minutesAllowance = findViewById(R.id.homeYourPlanUsageMinutesAllowanceText);
                        TextView minutesUsed = findViewById(R.id.homeYourPlanUsageMinutesUsedText);

                        try {
                            dataAllowance.setText(response.getString("dataAllowance") + "GB");
                            dataUsed.setText(response.getString("dataUsed") + "GB used");

                            textsAllowance.setText(response.getString("textsAllowance"));
                            textsUsed.setText(response.getString("textsUsed") + " used");

                            minutesAllowance.setText(response.getString("minutesAllowance"));
                            minutesUsed.setText(response.getString("minutesUsed") + " used");
                        } catch (JSONException err) {

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "An error occurred when connecting to our services. Please check your internet connection and try again.",
                                Toast.LENGTH_LONG).show();
                    }
                }
        );
        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(getRequest);
    }


    public void setBillingInfo(){
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, "https://welsh-telecom.herokuapp.com/api/account/billing/currentPeriod", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        TextView billingTotal = findViewById(R.id.homeBillingTotalText);
                        TextView billingDateRange = findViewById(R.id.homeBillingDateRangeText);
                        DecimalFormat df = new DecimalFormat("0.00");

                        CustomDateFormatter customDateFormatter = new CustomDateFormatter();

                        try {

                            Float monthlyCharges = Float.parseFloat(response.getString("monthlyCharges"));
                            Float additionalCharges = Float.parseFloat(response.getString("additionalCharges"));
                            Float total = monthlyCharges + additionalCharges;
                            billingTotal.setText("£" + String.valueOf(df.format(total)));

                            billingDateRange.setText("From" + customDateFormatter.formatShortDate(response.getString("periodStart")) + " to " + customDateFormatter.formatShortDate(response.getString("periodEnd")));

                        } catch (JSONException | ParseException err) {

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "An error occurred when connecting to our services. Please check your internet connection and try again.",
                                Toast.LENGTH_LONG).show();
                    }
                }
        );
        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(getRequest);
    }





}