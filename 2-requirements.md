**Mobile Development 2021/22 Portfolio**
# Requirements

Your username: `c2003457`

Student ID: `2003457`

_Complete the information above and then enumerate your functional and non functional requirements below.__

**Functional Requirements**

- Show plan allowances for data, texts and minutes
- Show allowance usage for the current period
- Show bill (monthly plan charges and additional charges) as a total and separately for the current period
- Show bill totals for previous billing periods
- Clickable icon to download the full PDF bill breakdown for previous billing periods
- Button to dial the customer service team from the app
- Switch to enable/disable notifications for spending beyond the monthly plan amount and when a bill is ready to download
- Allow customers to log in in using their mobile phone number
- Send and validate one-time password SMS messages (OTP) to confirm the customers identity
- Show events in rewards section with an image, venue and date with distance from the user's current location
- Show downloaded tickets as a list
- Provide a notification testing platform to send bill ready and overspending notifications to emulator (For Developers)

**Non-Functional Requirements**

- The UI will maintain a consistent design for professionalism and ease-of-use
- Use a contrasting colour scheme to improve accessibility for colour-blind customers
- Make UI responsive using constraints so that content is readable on different mobile phone screen sizes (excludes tablets due to nature of application)
- Build using an Android SDK that will be compatible with at least 90% of Android mobile devices
- The app should not crash, freeze or similar
- No activity should take longer than 500ms seconds to load (APDEX frustration threshold)
- Protect account access after initial setup by requesting a 4-digit pin before entry
- Customers must not be able to edit their bill information
- Code should be broken down into smaller, individual methods where possible to increase reusability and improve testability
- Creation of a UI design specification document to help future developers to maintain a consistent, professional interface
- Creation of user manual detailing the features of the product
