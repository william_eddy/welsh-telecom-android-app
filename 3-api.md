**Mobile Development 2021/22 Portfolio**
# API description

Your username: c2003457

Student ID: 2003457


| Android API Feature | Use |
| ------ | ------ |
| Fragments | To achieve my requirement of keeping a consistent design, fragments will used to help reduce duplicate code and maintain a single point of maintenance. |
| Constraint Layouts | Constraint layouts will be primarily used as their positioning is relative to the viewport, increasing the responsiveness and compatibility across a wide-range of mobile devices. |
| Recycler Views | Recycler views allow for items to be dynamically loaded as the user scrolls through the list. This is preferred over using a standard ListView as it will prevent loading a large list of rewards or tickets at once which would otherwise slow down the application. |
| External Storage | Usage data, billing and account information will be retrieved from an API using Volley. Volley allows for requests to be made asynchronously, not disrupting the flow of the application and allows the customer to continue using the application. |
| Internal Storage (SQLite) | Downloaded tickets will be stored in a local SQLite database. I chose to store these locally rather than externally via the API so that the ticket could be accessed in a venue without a network connection. |
| Shared Preferences | App-specific data such as the PIN are stored in a key-value pair. While this could be stored in the SQLite database, it is much easier and quicker to access a single value as a shared preference. |


